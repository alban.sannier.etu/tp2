import Component from "./Component.js"

class Img extends Component{

	constructor(imgUrl){
		super(`img`,{name:`src`, value: imgUrl});
	}
};

export default Img;