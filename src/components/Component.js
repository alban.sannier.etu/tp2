class Component{
	tagName;
	childre;
	attribute;

	constructor(tagName,attribute,childre){
		this.tagName = tagName;
		this.attribute = attribute;
		this.childre = childre;
	}

	render(){
		if(this.childre == null || this.childre == undefined){
			if(this.attribute != null || this.attribute != undefined)
				return `<${this.tagName}  ${this.renderAttribute()}/> `
			return `<${this.tagName} />`;
		}
		//console.log(`<${this.tagName} ${this.renderAttribute()}>${this.childre}</${this.tagName}>`);
		return `<${this.tagName} ${this.renderAttribute()}>${this.childre}</${this.tagName}>`;
	}
	renderAttribute(){
        if(this.attribute != null || this.attribute != undefined)
		    return this.attribute.name + "='" + this.attribute.value + "'"
        return ""    
    }
};

export default Component;